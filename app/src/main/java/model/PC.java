package model;

/**
 * Класс реализующий компьютерную стратегию игры.
 */
class PC {
    /**
     * Размер поля.
     */
    static final int SIZE = GameModel.SIZE;
    /**
     * Сторона за которую играет человек.
     */
    Players player;
    /**
     * Сторона за которую играет компьютер.
     */
    Players pc;
    /**
     * Поле.
     */
    Players field[][];

    /**
     * Конструктор.
     * @param field поле.
     * @param player сторона за которую играет человек.
     */
    PC(Players field[][], Players player) {
        this.field = new Players[SIZE][SIZE];
        for (int i = 0; i < SIZE; ++i)
            for (int j = 0; j < SIZE; ++j)
                this.field[i][j] = field[i][j];

        this.player = player;
        pc = this.player.getEnemy();
    }

    /**
     * Получение следующего хода компьютера.
     * @return координаты хода.
     */
    Cell getRun() {
        Players res = player;
        Cell c = null;

        for (int i = 0; i < SIZE; ++i)
            for (int j = 0; j < SIZE; ++j)
                if (field[i][j] == Players.EMPTY) {
                    field[i][j] = pc;
                    Players w = rek(player);
                    field[i][j] = Players.EMPTY;
                    if (w == pc) return new Cell(i, j);
                    else if (w == Players.EMPTY) {
                        c = new Cell(i, j);
                        res = w;
                    }
                    else if (res != Players.EMPTY) c = new Cell(i, j);
                }

        return c;
    }

    /**
     * Рекурсивная функция поиска хода.
     * @param p игрок, который сейчас ход.
     * @return игрок, который победит.
     */
    Players rek(Players p) {
        Players win = getWin();

        if (isDraw() && win == Players.EMPTY) return Players.EMPTY;
        if (win != Players.EMPTY) return win;

        Players enemy = p.getEnemy();
        Players res = enemy;
        for (int i = 0; i < SIZE; ++i)
            for (int j = 0; j < SIZE; ++j)
                if (field[i][j] == Players.EMPTY) {
                    field[i][j] = p;
                    Players w = rek(enemy);
                    field[i][j] = Players.EMPTY;
                    if (w == p) return p;
                    else if (w == Players.EMPTY) res = Players.EMPTY;
                }
        return res;
    }

    /**
     * Определение победителя.
     * @return победитель.
     */
    private Players getWin() {
        if (getWin(Players.CROSS)) return Players.CROSS;
        else if (getWin(Players.BILL)) return Players.BILL;
        else return Players.EMPTY;
    }

    /**
     * Проверка на победителя.
     * @param p игрок.
     * @return true, если игрок p победил, иначе false.
     */
    private boolean getWin(Players p) {
        for (int i = 0; i < SIZE; ++i) {
            boolean flag = true;
            for (int j = 0; (j < SIZE) && flag; ++j)
                if (field[i][j] != p) flag = false;
            if (flag) return true;
        }

        for (int j = 0; j < SIZE; ++j) {
            boolean flag = true;
            for (int i = 0; (i < SIZE) && flag; ++i)
                if (field[i][j] != p) flag = false;
            if (flag) return true;
        }

        boolean flag = true;
        for (int k = 0; (k < SIZE) && flag; ++k)
            if (field[k][k] != p) flag = false;
        if (flag) return true;

        flag = true;
        for (int k = 0; (k < SIZE) && flag; ++k)
            if (field[k][SIZE - k - 1] != p) flag = false;
        return flag;
    }

    /**
     * Проверка ничьи.
     * @return true, если ничья.
     */
    private boolean isDraw() {
        for (int i = 0; i < SIZE; ++i)
            for (int j = 0; j < SIZE; ++j)
                if (field[i][j] == Players.EMPTY) return false;
        return true;
    }


    /**
     * Класс клетки.
     */
    public class Cell {
        /**
         * Строка.
         */
        final int row;
        /**
         * Столбец.
         */
        final int col;

        /**
         * Конструктор.
         * @param row строка.
         * @param col столбец.
         */
        Cell(int row, int col) {
            this.row = row;
            this.col = col;
        }
    }
}
