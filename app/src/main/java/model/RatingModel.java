package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.IntegerRes;
import android.support.annotation.NonNull;

/**
 * Класс активити «Рейтинг».
 */
public class RatingModel {
    /**
     * Максимальная длина имени.
     */
    final static int MAX_LEN_NAME = 10;
    /**
     * Кол-во пробелов между именем и очками.
     */
    final static int LEN_SPACE = 5;
    /**
     * Макс. число позиций рейтинга.
     */
    final static int NUM_MAX = 5;
    /**
     * Файл, в котором хранится рейтинг.
     */
    final static String FILE_NAME = "rating";

    /**
     * Получение рейтинга.
     * @param context контекст из которого вызывается метод.
     * @return рейтинг.
     */
    public static ArrayList<Record> getRecords(Activity context) {
        try {
            FileInputStream in = context.getApplicationContext().openFileInput(FILE_NAME);
            Scanner sc = new Scanner(in);
            int n = Integer.parseInt(sc.nextLine());
            ArrayList<Record> rec = new ArrayList<>();
            for (int i = 0; i < n; ++i) {
                rec.add(new Record(sc.nextLine(), sc.nextLine()));
            }
            sc.close();
            return rec;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /**
     * Добавление записи в рейтинг.
     * @param context котекст из которого вызывается метод.
     * @param rec добавляемая запись.
     */
    public static void putRecord(Activity context, Record rec) {
        ArrayList<Record> records = getRecords(context);
        records.add(rec);
        Collections.sort(records);
        try {
            FileOutputStream out = context.getApplicationContext().openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            PrintWriter pw = new PrintWriter(out);
            int n = Math.min(NUM_MAX, records.size());
            pw.write(Integer.toString(n) + "\n");

            for(int i = 0; i < n; ++i) {
                Record r = records.get(i);
                pw.write(r.name + "\n");
                pw.write(Integer.toString(r.score) + "\n");
            }

            pw.close();
        } catch (Exception e) {}
    }


    /**
     * Запись рейтинга.
     */
    public static class Record implements  Comparable<Record> {
        /**
         * Имя.
         */
        final String name;
        /**
         * Кол-во ходов.
         */
        final int score;

        /**
         * Конструктор.
         * @param name имя.
         * @param score кол-во ходов.
         */
        public Record(String name, String score) {
            this.name = name;
            this.score = Integer.parseInt(score);
        }

        /**
         * Метод сравнения.
         * @param record запись, с которой сравниваем.
         * @return  -1, если текущая запись меньше record.
         *           0, если текущая запись равно  record.
         *          +1, если текущая запись больше record.
         */
        @Override
        public int compareTo(@NonNull Record record) {
            if (score < record.score) return -1;
            else if (score > record.score) return 1;
            else return name.compareTo(record.name);
        }

        /**
         * Преобразование к строке.
         * @return строковое представление записи.
         */
        @Override
        public String toString() {
            String str = name;
            while (str.length() != MAX_LEN_NAME + LEN_SPACE) str = str + " ";
            str = str + Integer.toString(score);
            return str;
        }
    }

}
