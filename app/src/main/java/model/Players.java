package model;

/**
 * Тип игрока.
 */
public enum Players {
    /**
     * Ни тот, ни другой.
     */
    EMPTY,
    /**
     * Крестики
     */
    CROSS,
    /**
     * Нолики
     */
    BILL;

    /**
     * Преобразование типа String к Players.
     * @param str данные.
     * @return преобразованной значение.
     */
    public static Players parseFromString(String str) {
        if (str.compareTo(CROSS.toString()) == 0) return CROSS;
        else if (str.compareTo(BILL.toString()) == 0) return BILL;
        else return EMPTY;
    }

    /**
     * Получение противника текущего игрока.
     * @return противник игрока.
     */
    public Players getEnemy() {
        switch (this) {
            case CROSS: return BILL;
            case BILL: return CROSS;
            default: return EMPTY;
        }
    }

    /**
     * Преобразование к строке.
     * @return строковое представление объекта.
     */
    public String toString() {
        switch (this) {
            case CROSS: return "X";
            case BILL: return "O";
            default: return "";
        }
    }
}
