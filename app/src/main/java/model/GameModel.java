package model;

/**
 * Класс описывающий игру.
 */
public class GameModel {
    /**
     * Размер поля.
     */
    public final static int SIZE = 3;

    /**
     * Кол-во ходов.
     */
    int steps;

    /**
     * Текущий игрок.
     */
    Players player;
    /**
     * Поле.
     */
    Players field[][];

    /**
     * Конструктор.
     */
    public GameModel() {
        field = new Players[SIZE][SIZE];
        for (int i = 0; i < SIZE; ++i)
            for (int j = 0; j < SIZE; ++j)
                field[i][j] = Players.EMPTY;

        player = Players.CROSS;
        steps = 0;
    }

    /**
     * Обработка хода игрока.
     * @param row строка хода игрока.
     * @param col столбец хода игрока.
     */
    public void click(int row, int col) {
        if (field[row][col] == Players.EMPTY) {
            field[row][col] = player;
            player = player.getEnemy();
            ++steps;
        }
    }

    /**
     * Получения значения клетки поля.
     * @param row строка.
     * @param col столбец.
     * @return значение клекти (row, col) поля.
     */
    public Players getCell(int row, int col) {
        return field[row][col];
    }

    /**
     * Получение победителя.
     * @return победитель.
     */
    public Players getWin() {
        if (getWin(Players.CROSS)) return Players.CROSS;
        else if (getWin(Players.BILL)) return Players.BILL;
        else return Players.EMPTY;
    }

    /**
     * Ход компьютера.
     */
    public void runPC() {
        PC pc = new PC(field, player.getEnemy());
        PC.Cell cell = pc.getRun();
        click(cell.row, cell.col);
    }

    /**
     * Проверка игрока на победителя.
     * @param p игрок.
     * @return true, если игрок p победил, иначе false.
     */
    boolean getWin(Players p) {
        for (int i = 0; i < SIZE; ++i) {
            boolean flag = true;
            for (int j = 0; (j < SIZE) && flag; ++j)
                if (field[i][j] != p) flag = false;
            if (flag) return true;
        }

        for (int j = 0; j < SIZE; ++j) {
            boolean flag = true;
            for (int i = 0; (i < SIZE) && flag; ++i)
                if (field[i][j] != p) flag = false;
            if (flag) return true;
        }

        boolean flag = true;
        for (int k = 0; (k < SIZE) && flag; ++k)
            if (field[k][k] != p) flag = false;
        if (flag) return true;

        flag = true;
        for (int k = 0; (k < SIZE) && flag; ++k)
            if (field[k][SIZE - k - 1] != p) flag = false;
        return flag;
    }

    /**
     * Проверка на ничью.
     * @return true, если ничья, иначе false.
     */
    public boolean isDraw() {
        for (int i = 0; i < SIZE; ++i)
            for (int j = 0; j < SIZE; ++j)
                if (field[i][j] == Players.EMPTY) return false;
        return true;
    }

    /**
     * Метод получения игрока, чей сейчас ход.
     * @return игрок, который должен сейчас ходить.
     */
    public Players getPlayer() {
        return player;
    }

    /**
     * Получение числа ходов.
     * @return число ходов.
     */
    public int getSteps() {
        return steps;
    }
}
