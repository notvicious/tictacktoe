package activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.intsashka.tic_tac_toe.R;

import model.Players;
import model.RatingModel;

/**
 * Класс активити "Сохранение результата".
 */
public class SaveResultActivity extends AppCompatActivity {

    /**
     * Метод, который вызывается при создании активити.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_result);
    }

    /**
     * Метод, который обрабатываем событие клик по кнопке "Ввод".
     * @param view элемент, который сгенерировал событие.
     */
    public void onClickAdd(View view){
        EditText ed = (EditText) findViewById(R.id.editTextName);
        if (ed.getText().toString().trim().length() != 0) {
            Intent intent = getIntent();
            String score = intent.getStringExtra(FieldActivity.PARAMETR_SCORE);
            RatingModel.putRecord(this, new RatingModel.Record(ed.getText().toString(), score));

            Intent inte = new Intent(this, RatingActivity.class);
            this.startActivity(inte);
            finish();
        }
    }
}
