package activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.intsashka.tic_tac_toe.R;

import model.GameModel;
import model.Players;

/**
 * Класс активити "Игра".
 */
public class FieldActivity extends AppCompatActivity {
    /**
     * Ключ параметра, по которому передается сторона за которую играет человек.
     */
    public static String PARAMETR_PLAYER = "PLAYER";

    /**
     * Ключ параметра, по которому передается количество ходов.
     */
    public static String PARAMETR_SCORE = "SCORE";

    /**
     * Игра.
     */
    GameModel game;
    /**
     * Сторона за которую играет человек.
     */
    Players player;
    /**
     * Массив id кнопок поля.
     */
    int field[][];

    /**
     * Конструктор.
     */
    public FieldActivity() {
        super();
        this.player = Players.CROSS;
        game = new GameModel();

        field = new int[GameModel.SIZE][GameModel.SIZE];
        field[0][0] = R.id.button_cell00;
        field[0][1] = R.id.button_cell01;
        field[0][2] = R.id.button_cell02;
        field[1][0] = R.id.button_cell10;
        field[1][1] = R.id.button_cell11;
        field[1][2] = R.id.button_cell12;
        field[2][0] = R.id.button_cell20;
        field[2][1] = R.id.button_cell21;
        field[2][2] = R.id.button_cell22;
    }

    /**
     * Метод, который вызывается при создании активити.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_field);
        Intent intent = getIntent();
        player = Players.parseFromString(intent.getStringExtra(PARAMETR_PLAYER));
        if (player == Players.BILL) {
            game.runPC();
            draw();
        }
    }

    /**
     * Метод, который обрабатывает событие клик по полю.
     * @param view элемент, который сгенерировал событие.
     */
    public void onClickField(View view) {
        if (game.getWin() == Players.EMPTY && !game.isDraw()) {
            int id = view.getId();
            for (int i = 0; i < GameModel.SIZE; ++i)
                for (int j = 0; j < GameModel.SIZE; ++j)
                    if (field[i][j] == id) {
                        game.click(i, j);
                        draw();
                        if(!checkWin() && game.getPlayer() != player) {
                            game.runPC();
                            draw();
                            checkWin();
                        }
                    }
        }
    }

    /**
     * Метод, который обрабатываем событие клик по кнопке "в главное меню".
     * @param view элемент, который сгенерировал событие.
     */
    public void onClickRestart(View view) {
        Intent myIntent = new Intent(this, NewGameActivity.class);
        this.startActivity(myIntent);
        finish();
    }

    /**
     * Метод отрисовки поля.
     */
    void draw() {
        for (int i = 0; i < GameModel.SIZE; ++i)
            for (int j = 0; j < GameModel.SIZE; ++j) {
                Button but = (Button) findViewById(field[i][j]);
                but.setText(game.getCell(i,j).toString());
            }
    }

    /**
     * Метод проверки конца игры.
     * @return true, если игра закончилась.
     */
    boolean checkWin() {
        Players win = game.getWin();
        if (win != Players.EMPTY || game.isDraw()) {
            String mess;
            if (game.isDraw() && win == Players.EMPTY) mess = getResources().getString(R.string.mess_draw);
            else if (win == player) mess = getResources().getString(R.string.mess_win);
            else mess = getResources().getString(R.string.mess_lose);
            mess += "\n" + getString(R.string.num_steps) + game.getSteps();

            Button but = (Button) findViewById(R.id.button_enter_name);
            but.setVisibility(View.VISIBLE);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.title_dialog))
                    .setMessage(mess)
                    .setCancelable(false)
                    .setNegativeButton(getResources().getString(R.string.button_ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            return true;
        }
        return false;
    }

    /**
     * Метод, который обрабатываем событие клик по кнопке "Сохранить результат".
     * @param view элемент, который сгенерировал событие.
     */
    public void onClickEnterName(View view) {
        Intent myIntent = new Intent(this, SaveResultActivity.class);
        myIntent.putExtra(FieldActivity.PARAMETR_SCORE, Integer.toString(game.getSteps()));
        this.startActivity(myIntent);
        finish();
    }


}
