package activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.intsashka.tic_tac_toe.R;

import java.util.ArrayList;

import model.RatingModel;

/**
 * Класс активити "Рейтинг".
 */
public class RatingActivity extends AppCompatActivity {

    /**
     * Метод, который вызывается при создании активити.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        ArrayList<RatingModel.Record> rec = RatingModel.getRecords(this);
        LinearLayout lst = (LinearLayout) findViewById(R.id.ratingList);

        int it = 1;
        for (RatingModel.Record r : rec) {
            TextView tv = (TextView) getLayoutInflater().inflate(R.layout.text_view_styled, null);
            tv.setText(r.toString());
            lst.addView(tv, it++);
        }
    }

    /**
     * Метод, который обрабатываем событие клик по кнопке "в главное меню".
     * @param view элемент, который сгенерировал событие.
     */
    public void onClickBackMain(View view) {
        Intent myIntent = new Intent(this, NewGameActivity.class);
        this.startActivity(myIntent);
        finish();
