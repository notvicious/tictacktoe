package activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.intsashka.tic_tac_toe.R;

import model.Players;

/**
 * Класс активити "Главное меню".
 */
public class NewGameActivity extends AppCompatActivity {

    /**
     * Метод, который вызывается при создании активити.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game);
    }

    /**
     * Обработки нажатия на кнопку "Играть крестиками" или "Играть ноликами".
     * @param view элемент, который сгенерировал событие.
     */
    public void onClickNewGame(View view) {
        Players player;
        switch (view.getId()) {
            case R.id.button_play_crosses: player = Players.CROSS; break;
            case R.id.button_play_bills:   player = Players.BILL; break;
            default:                       player = Players.EMPTY;
        }
        Intent intent = new Intent(NewGameActivity.this, FieldActivity.class);
        intent.putExtra(FieldActivity.PARAMETR_PLAYER, player.toString());
        this.startActivity(intent);
        finish();
    }

    /**
     * Обработка нажатия на кнопку "Рейтинг".
     * @param view элемент, который сгенерировал событие.
     */
    public void onClickToRating(View view) {
        Intent intent = new Intent(NewGameActivity.this, RatingActivity.class);
        this.startActivity(intent);
        finish();
    }

    /**
     * Обработка нажатия на кнопку "Выход".
     * @param view элемент, который сгенерировал событие.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void onClickExit(View view) {
        finishAndRemoveTask();
    }
}
